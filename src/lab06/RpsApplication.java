package lab06;
// Lots of javaFX imports.
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

/**
 * The main visual application. (No more CLI!)
 * @author Danilo Zhu 1943382
 */
public class RpsApplication extends Application {
	private RpsGame rpsGame;

	public void start(Stage stage) {
		rpsGame = new RpsGame();
		Group root = new Group();

		// scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300);
		scene.setFill(Color.BLACK);

		// associate scene to stage and show
		stage.setTitle("Rock Paper Scissors");
		stage.setScene(scene);

		VBox overall = new VBox(); // The VBox that contains everything inside it.

		HBox buttons = new HBox(); // The HBox containing the choice buttons.
		Button rockButton = new Button("Rock");
		Button paperButton = new Button("Paper");
		Button scissorsButton = new Button("Scissors");

		overall.getChildren().add(buttons);
		buttons.getChildren().addAll(rockButton, paperButton, scissorsButton);

		HBox textFields = new HBox(); // The HBox containing the total wins, losses, ties and the message output by the application.
		TextField welcome = new TextField("Welcome! Stats: ");
		TextField winsField = new TextField("Wins: " + rpsGame.getWins());
		TextField tiesField = new TextField("Ties: " + rpsGame.getTies());
		TextField lossesField = new TextField("Losses: " + rpsGame.getLosses());

		welcome.setPrefWidth(200); // Set the preferred width of the welcome TextField to 200px.

		overall.getChildren().add(textFields);
		textFields.getChildren().addAll(welcome, winsField, tiesField, lossesField);

		root.getChildren().add(overall);
		
		RpsChoice rockChoice = new RpsChoice(welcome, winsField, tiesField, lossesField, "Rock", rpsGame);
		RpsChoice paperChoice = new RpsChoice(welcome, winsField, tiesField, lossesField, "Paper", rpsGame);
		RpsChoice scissorsChoice = new RpsChoice(welcome, winsField, tiesField, lossesField, "Scissors", rpsGame);
		
		rockButton.setOnAction(rockChoice);
		paperButton.setOnAction(paperChoice);
		scissorsButton.setOnAction(scissorsChoice);
		
		stage.show();
	}

	public static void main(String[] args) {
		Application.launch(args);
	}
}
