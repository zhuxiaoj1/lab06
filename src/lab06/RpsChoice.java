package lab06;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

/**
 * This class contains the constructor for an RpsChoice object which is then
 * used to display statistics on the application main screen.
 * 
 * @author Danilo Zhu 1943382
 */
public class RpsChoice implements EventHandler<ActionEvent> {
	private TextField message;
	private TextField wins;
	private TextField ties;
	private TextField losses;
	private String choice;
	private RpsGame gameObj;

	/**
	 * Constructor of the RpsChoice object.
	 * 
	 * @param message The TextField where the message will be output.
	 * @param wins    The Text Field where the total wins will be output.
	 * @param ties    The Text Field where the total ties will be output.
	 * @param losses  The Text Field where the total losses will be output.
	 * @param choice  String that is the choice of the player.
	 * @param gameObj RpsGame object.
	 */
	public RpsChoice(TextField message, TextField wins, TextField ties, TextField losses, String choice,
			RpsGame gameObj) {
		this.message = message;
		this.wins = wins;
		this.ties = ties;
		this.losses = losses;
		this.choice = choice;
		this.gameObj = gameObj;
	}

	/**
	 * This method handles the moment when the player clicks on a choice button.
	 * 
	 * @param event ActionEvent caused by the clicking of one of the choice buttons
	 *              in the main application.
	 */
	@Override
	public void handle(ActionEvent event) {
		String result = gameObj.playRound(choice);
		message.setText(result);

		wins.setText("Wins: " + gameObj.getWins());
		losses.setText("Losses: " + gameObj.getLosses());
		ties.setText("Ties: " + gameObj.getTies());
	}
}
