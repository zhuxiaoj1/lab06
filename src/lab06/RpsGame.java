package lab06;

import java.util.*;

/**
 * This class is contains the variables for wins, losses, ties and a method
 * playRound() which handles incrementing the variables.
 * 
 * @author Danilo Zhu 1943382
 */
public class RpsGame {
	private int wins = 0, losses = 0, ties = 0;
	Random ra = new Random();

	public int getWins() {
		return wins;
	}

	public int getLosses() {
		return losses;
	}

	public int getTies() {
		return ties;
	}

	/**
	 * This method plays a round of Rock, Paper, Scissors following its ancestral laws.
	 * @param choice The choice of the player whether "Rock", "Paper" or "Scissors".
	 * @return String representing the outcome of the round.
	 */
	public String playRound(String choice) {
		String aiChoice = "", result = "";
		int randChoice = ra.nextInt(3);

		switch (randChoice) {
		case 0:
			aiChoice = "Rock";
			break;
		case 1:
			aiChoice = "Paper";
			break;
		case 2:
			aiChoice = "Scissors";
			break;
		}

		if (aiChoice.equals(choice)) {
			result = "tie";
			ties++;
		} else if ((aiChoice.equals("Rock") && choice.equals("Scissors")) || (aiChoice.equals("Paper") && choice.equals("Rock")) || (aiChoice.equals("Scissors") && choice.equals("Paper"))) {
			result = "loss";
			losses++;
		} else {
			result = "win";
			wins++;
		}

		return "The AI randomly chose: " + aiChoice + " and the result was a: " + result + " for you!";
	}
}
